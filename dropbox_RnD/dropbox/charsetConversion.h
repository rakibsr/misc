#include <stdlib.h>     /* for realloc() and free() */
#include <string.h>     /* for memset() */
#include <errno.h>      /* for errno */


char *latin9_to_utf8(const char *const string);
char *utf8_to_latin9(const char *const string);