﻿/* Created in 2015/2016.
For RnD purpose, getting dropbox data. Was not completed
Author: rakibdana@gmail.com/rakibsr@gmail.com
*/

#include<stdio.h>
#include<dos.h>
#include<stdlib.h>
#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<numeric>
#include<vector>
#include<map>
#include<iterator>
#include<string.h>
#include<string>

#include <iostream>
#include <fstream>
#include <string>
#include "base64.h"
#include <curl.h> //your directory may be different

#include "protoDecoder.c"
#include "charsetConversion.h"

#include <fcntl.h>

#define dbg 1

using namespace std;

string data; //will hold the url's contents
vector<char> bytes;
fstream output("out.txt", ios::out | ios::binary);

string appName = "tizentesting";
string appKey = "5gdith1l1gjk746";
string appSecret = "u4mjvc6sca1iomp";
string tokenKey = "";
string tokenSecret = "";

string imgURL = "http://images.nationalgeographic.com/wpf/media-live/photos/000/007/cache/siberian-tiger_707_600x450.jpg";

size_t writeCallbackBytes(char* buf, size_t size, size_t nmemb, void* up)
{ 
	bytes.clear();
	bytes.insert(bytes.end(),buf,buf+size*nmemb);// copy bytes to vector
    return size*nmemb; //tell curl how many bytes we handled
}

size_t writeCallback(char* buf, size_t size, size_t nmemb, void* up)
{ 
    for (int c = 0; c<size*nmemb; c++)
    {
        data.push_back(buf[c]);
    }
    return size*nmemb; //tell curl how many bytes we handled
}

void print()
{
	//cout << "################ RESPONSE ################" << endl << data << endl;

	//freopen("out.txt", "w+", stdout);
	output << data << endl ;
}

void simpleCurl()
{
	CURL* curl; //curl handler

    curl_global_init(CURL_GLOBAL_ALL); //windows and ssl enabled


    curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_URL, imgURL.c_str());//http://www.google.com/
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);
  //  curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); //tell curl to output its progress

    curl_easy_perform(curl);

	fstream output("out.jpg", ios::out | ios::binary);
	output << data ;
	output.close();

    curl_easy_cleanup(curl);
    curl_global_cleanup();
}




void parseToken(string response)
{
	// inlude error check later
	string toksecstring = "oauth_token_secret=";
	string tokstring = "oauth_token=";

	size_t foundTokenSecret = response.find(toksecstring);
  
	if (foundTokenSecret == std::string::npos) {
		output << "Not found" <<endl;
	} else {
		size_t foundTokenKey = response.find(tokstring);
		size_t tokenSecretLen = foundTokenKey - toksecstring.length();
		tokenSecret = response.substr(toksecstring.length(), tokenSecretLen);
		tokenKey = response.substr(foundTokenKey + tokstring.length());

		#ifdef dbg
		output << "token = " << tokenKey << endl << "token_secret = " << tokenSecret << endl;
		#endif
	}

    return;
}

void GetRequestToken()
{
	char *url = "https://api.dropboxapi.com/1/oauth/request_token";

	CURL* curl; //curl handler
	struct curl_slist *slist = NULL;

	curl_global_init(CURL_GLOBAL_ALL); //windows and ssl enabled
	curl = curl_easy_init();

	//string consumerKey= "oauth_consumer_key=\"" + base64_encode(reinterpret_cast<const unsigned char*>(appKey.c_str()), appKey.length()) + "\", ";
	string consumerKey= "oauth_consumer_key=\"" + appKey + "\", ";
	//string consumerSecret = "oauth_signature=\"" + base64_encode(reinterpret_cast<const unsigned char*>(appSecret.c_str()), appSecret.length()) + "&\"";
	string consumerSecret = "oauth_signature=\"" + appSecret + "&\"";
	string header = "Authorization: OAuth oauth_version=\"1.0\", oauth_signature_method=\"PLAINTEXT\", oauth_callback=\"oob\", " + consumerKey + consumerSecret;

	slist = curl_slist_append(slist, header.c_str());

    curl_easy_setopt(curl, CURLOPT_URL, url); 
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	//curl_easy_setopt(curl, CURLOPT_USERPWD, "mehedi@dhrubokinfotech.com:Loopy1123");
	//curl_easy_setopt(curl, CURLOPT_PORT,443); 
	//curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	//curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
	curl_easy_setopt(curl,  CURLOPT_POST, 1);
	curl_easy_setopt(curl,  CURLOPT_POSTFIELDS, "");
	curl_easy_setopt(curl,  CURLOPT_POSTFIELDSIZE, 0);
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); //tell curl to output its progress

	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);

    curl_easy_perform(curl);

	print();

    curl_easy_cleanup(curl);
    curl_global_cleanup();

}

void GetAuthorize()
{
	char *url = "https://www.dropbox.com/1/oauth/authorize";

	CURL* curl; //curl handler
	struct curl_slist *slist = NULL;

	curl_global_init(CURL_GLOBAL_ALL); //windows and ssl enabled
	curl = curl_easy_init();

	string consumerKey= "oauth_consumer_key=\"" + appKey + "\", ";
	string oAuthToken = " oauth_token=\"" + tokenKey + "\", ";
	string signature = "oauth_signature=\"" + appSecret + "&" + tokenSecret+ "\"";
	string header = "Authorization: OAuth oauth_version=\"1.0\", oauth_signature_method=\"PLAINTEXT\", " + consumerKey + oAuthToken + signature;

	slist = curl_slist_append(slist, header.c_str());

    curl_easy_setopt(curl, CURLOPT_URL, url); 
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
	//curl_easy_setopt(curl,  CURLOPT_POST, 1);
	//curl_easy_setopt(curl,  CURLOPT_POSTFIELDS, "");
	//curl_easy_setopt(curl,  CURLOPT_POSTFIELDSIZE, 0);
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); //tell curl to output its progress

	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);

    curl_easy_perform(curl);

	print();

    curl_easy_cleanup(curl);
    curl_global_cleanup();

}

void GetAccessToken()
{
	char *url = "https://api.dropboxapi.com/1/oauth/access_token";


	CURL* curl; //curl handler
	struct curl_slist *slist = NULL;

	curl_global_init(CURL_GLOBAL_ALL); //windows and ssl enabled
	curl = curl_easy_init();

	string consumerKey= "oauth_consumer_key=\"" + appKey + "\", ";
	string oAuthToken = "oauth_token=\"" + tokenKey + "\", ";
	string signMethod = "oauth_signature_method=\"PLAINTEXT\", ";
	string signature = "oauth_signature=\"" + appSecret + "&" + tokenSecret+ "\"";//, oauth_signature_method=\"PLAINTEXT\", " 
	string header = "Authorization:OAuth oauth_version=\"1.0\", oauth_verifier=\"1111\", "+ consumerKey + oAuthToken + signMethod + signature;
	 
	slist = curl_slist_append(slist, header.c_str());

    curl_easy_setopt(curl, CURLOPT_URL, url); 
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	//curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
	curl_easy_setopt(curl,  CURLOPT_POST, 1);
	curl_easy_setopt(curl,  CURLOPT_POSTFIELDS, "");
	curl_easy_setopt(curl,  CURLOPT_POSTFIELDSIZE, 0);
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); //tell curl to output its progress

	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);

    curl_easy_perform(curl);

	print();

    curl_easy_cleanup(curl);
    curl_global_cleanup();

}


string _host="p29-setup.icloud.com:443";//"p34-setup.icloud.com:443"; // Never put 'https:\\' on host. 

void fetchQuotaDetails()
{
	char *url = "https://p29-quota.icloud.com:443/quotaservice/external/ios/543445470/getQuotaDetails"; //_quotaInfoURL.replace("Info", "Details");

	CURL* curl; //curl handler
	struct curl_slist *slist=NULL;

	curl_global_init(CURL_GLOBAL_ALL); //windows and ssl enabled
	curl = curl_easy_init();

	slist = curl_slist_append(slist, "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/534.57.7 (KHTML, like Gecko) iCloudSettings/1.0");//changed
	slist = curl_slist_append(slist, "X-Mme-Client-Info: <iPod4,1> <iPhone OS;5.0.1;9A405> <com.apple.AppleAccount/1.0 (com.apple.Preferences/1.0)>");
	
	string secMme = "habijabi"; //again change is here
	string authBasic= "Authorization: Basic "+ base64_encode(reinterpret_cast<const unsigned char*>(secMme.c_str()), secMme.length());
	slist = curl_slist_append(slist, authBasic.c_str());
	cout<<authBasic;

	slist = curl_slist_append(slist, "some");

	slist = curl_slist_append(slist,"Origin: www.icloud.com");
	slist = curl_slist_append(slist,"Referer: www.icloud.com");

	string host = "Host: "+_host;
	slist = curl_slist_append(slist, host.c_str()); // Never put 'https:\\' on host. 

	slist = curl_slist_append(slist,"Content-type: application/json");

    curl_easy_setopt(curl, CURLOPT_URL, url); 
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);

	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_easy_setopt(curl, CURLOPT_USERPWD, "mehedi@dhrubokinfotech.com:Loopy1123");
	curl_easy_setopt(curl, CURLOPT_PORT,443); 
	curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);

    curl_easy_perform(curl);

	//freopen("out.txt", "w+", stdout);
		
	//fstream output("out.txt", ios::out | ios::binary);
	//output << data ;
	//output.close();
  //  cin.get();

    curl_easy_cleanup(curl);
    curl_global_cleanup();

}

void enumerateSnapshot()
{
	CURL *curl;
    CURLcode res;
	struct curl_slist *slist=NULL;

	string url = "haha";

	//slist = curl_slist_append(slist, "Depth: 1");
	curl_global_init(CURL_GLOBAL_ALL);

    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str()); 
		//curl_easy_setopt(curl, CURLOPT_USERPWD, "siraj.zafl@gmail.com:JKoolip&123");
		curl_easy_setopt(curl, CURLOPT_USERPWD, "mehedi@dhrubokinfotech.com:Loopy1123");
		curl_easy_setopt(curl, CURLOPT_PORT,443); 
		curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	//	curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING,""); //not supported in this version of curl

		string authBasic= "Authorization: Basic "+ base64_encode(reinterpret_cast<const unsigned char*>("dd"), 2);
		slist = curl_slist_append(slist,"X-Mme-Client-Info: <iPhone3,1> <iPhone OS;6.1.3;10B329> <com.apple.AppleAccount/1.0 (com.apple.backupd/(null))>");
		slist = curl_slist_append(slist,"Host: p05-mobilebackup.icloud.com");
        slist = curl_slist_append(slist,"Accept: application/vnd.com.apple.mbs+protobuf");
        slist = curl_slist_append(slist,"X-Apple-MBS-Protocol-Version: 1.7");
		slist = curl_slist_append(slist,"Accept-Encoding: charset=ISO-8859-1"); // not sure now

		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);

		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);

        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
		curl_global_cleanup();

		//fstream output("out.txt", ios::out | ios::binary);
		//output << data ;
		//output.close();
    }
}

void prefetchAuthForFiles()
{
	string theSnapID="1";
	string offset="0";//int
	
	CURL *curl;
    CURLcode res;
	struct curl_slist *slist=NULL;

	char filesPayload[] = {0x16,0x14,0x2a,0x5b,0xe9,0x1c,0x4b,0xb3,0xa2,0xa1,0x9f,0x2e,0xb9,0x74,0x58,0x2e,0x16,0x00,0x12,0x84,0x63,0x16,0x14,0x8b,0xc4,0xa3,0xf9,0xa9,0x1b,0xe6,0x4b,0x7e,0xde,0x88,0x02,0x65,0x57,0x72,0xc3,0x1b,0xf8,0x18,0xbc,0x16,0x14,0xc9,0xcf,0x4a,0x1e,0xee,0xdf,0x55,0x5e,0x32,0x38,0x72,0x4d,0xa0,0x9a,0xe8,0xa5,0x4e,0xda,0xcd,0x73,0x16,0x14,0xd0,0x5b,0xe5,0x10,0x65,0x52,0x55,0x60,0x36,0xcd,0x29,0xc6,0xdb,0xc6,0x64,0x67,0x41,0xe0,0xa8,0xa6,0x16,0x14,0xf1,0x27,0x00,0xdc,0x87,0xd7,0xe9,0x79,0x1a,0x33,0xfd,0xf1,0x09,0x63,0x80,0xd7,0xb8,0x11,0x67,0x6e};
	//string f="FgoUKlvpHEuzoqGfLrkKdFguFgAShGMWChSLxKP5qRvmS37eiAJlV3LDG/gYvBYKFMnPSh7u31VeMjhyTaCa6KVO2s1zFgoU0FvlEGVSVWA2zSnG28ZkZ0HgqKYWChTxJwDch9fpeRoz/fEJY4DXuBFnbg==";
	char* baseEn ="S2x2RHFSeEx3clBDb3NLaHdwOHV3cmwwV0M0Vzc3Kzk3Nys5RXNLRVl4WVV3b3ZEaE1Lanc3bkNxUnZEcGt0K3c1N0NpQUpsVjNMRGd4dkR1QmpDdkJZVXc0bkRqMG9ldzY3RG4xVmVNamh5VGNLZ3dwckRxTUtsVHNPYXc0MXpGaFREa0Z2RHBSQmxVbFZnTnNPTktjT0d3NXZEaG1SblFjT2d3cWpDcGhZVXc3RW43Nys5NzcrOXc1ekNoOE9YdzZsNUdqUER2Y094Q1dQQ2dNT1h3cmdSWjI0PQ==";

	byte b[]={22, 20, 42, 91, -61, -87, 28, 75, -62, -77, -62, -94, -62, -95, -62, -97, 46, -62, -71, 116, 88, 46, 22, 0, 18, -62, -124, 99, 22, 20, -62, -117, -61, -124, -62, -93, -61, -71, -62, -87, 27, -61, -90, 75, 126, -61, -98, -62, -120, 2, 101, 87, 114, -61, -125, 27, -61, -72, 24, -62, -68, 22, 20, -61, -119, -61, -113, 74, 30, -61, -82, -61, -97, 85, 94, 50, 56, 114, 77, -62, -96, -62, -102, -61, -88, -62, -91, 78, -61, -102, -61, -115, 115, 22, 20, -61, -112, 91, -61, -91, 16, 101, 82, 85, 96, 54, -61, -115, 41, -61, -122, -61, -101, -61, -122, 100, 103, 65, -61, -96, -62, -88, -62, -90, 22, 20, -61, -79, 39, 0, -61, -100, -62, -121, -61, -105, -61, -87, 121, 26, 51, -61, -67, -61, -79, 9, 99, -62, -128, -61, -105, -62, -72, 17, 103, 110};
	string url ="https://p29-mobilebackup.icloud.com:443/mbs/543445470/389673d4e9fbfbb546b548bbc381d61724b87cb6/1/getFiles"; 
	//_backupServerURL+"/mbs/"+_dsPrsID+"/"+_backupFirstEntryID+"/"+theSnapID+"/getFiles";
	curl_global_init(CURL_GLOBAL_ALL);

    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str()); 
		curl_easy_setopt(curl, CURLOPT_USERPWD, "mehedi@dhrubokinfotech.com:Loopy1123");//mehedi@dhrubokinfotech.com:Loopy1123
		curl_easy_setopt(curl, CURLOPT_PORT,443); 
		curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	//	curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING,""); //not supported in this version of curl

		slist = curl_slist_append(slist, "User-Agent: Backup/6.1.3 (10B329; iPhone3,1)");
		string secMme = "aaa"; //same as fetchQuotaDetails(),enumerateSnapshot()
		string authBasic= "Authorization: Basic NTQzNDQ1NDcwOkFRQUFBQUJWSjA1SmVCYkc1eEp4bStudVdyVmt4ZGRkUXdxSkVRVT0=";
		//"Authorization: Basic "+ base64_encode(reinterpret_cast<const unsigned char*>(secMme.c_str()), secMme.length());
		cout<<authBasic;
		slist = curl_slist_append(slist, authBasic.c_str());

		slist = curl_slist_append(slist,"X-Mme-Client-Info: <iPhone3,1> <iPhone OS;6.1.3;10B329> <com.apple.AppleAccount/1.0 (com.apple.backupd/(null))>");
		slist = curl_slist_append(slist,"Host: https://p29-mobilebackup.icloud.com:443");//Host: p05-mobilebackup.icloud.com
        slist = curl_slist_append(slist,"Accept: application/vnd.com.apple.mbs+protobuf");
        slist = curl_slist_append(slist,"X-Apple-MBS-Protocol-Version: 1.7");
		slist = curl_slist_append(slist,"Content-type:application/vnd.com.apple.mbs+protobuf");
		slist = curl_slist_append(slist,"Accept-Encoding: charset=ISO-8859-1"); // not sure now

		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);

		curl_easy_setopt(curl,  CURLOPT_POST, 1);
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_easy_setopt(curl,  CURLOPT_POSTFIELDS, filesPayload);

		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);

        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
		curl_global_cleanup();

	//	freopen("out.txt", "w+", stdout);
		cout << data <<endl ;
		cout<<res;
    }
}

void Prop( char *url, string propData)
{
	CURL* curl; //curl handler
	struct curl_slist *slist=NULL;

	slist = curl_slist_append(slist, "Depth: 1");
	slist = curl_slist_append(slist, "Content-Type: text/xml; charset='UTF-8'");
//	slist = curl_slist_append(slist, "User-Agent:  iCloud.exe (unknown version) CFNetwork/520.2.6");
//	slist = curl_slist_append(slist, "X-Mme-Client-Info: <PC> <Windows; 6.1.7601/SP1.0; W> <com.apple.AOSKit/88>");
//	slist = curl_slist_append(slist, "Accept-Language: en-US");
//	slist = curl_slist_append(slist, "Authorization: Basic");

    curl_global_init(CURL_GLOBAL_ALL); //windows and ssl enabled

    curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_URL, url); //"https://p34-contacts.icloud.com/1260330658/carddavhome")
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
	curl_easy_setopt(curl, CURLOPT_HEADER, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);

	curl_easy_setopt(curl, CURLOPT_USERPWD, "siraj.zafl@gmail.com:JKoolip&123");
	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PROPFIND");
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, propData.c_str()); //
			
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);

    curl_easy_perform(curl);

    cout << endl << data << endl;
    cin.get();

    curl_easy_cleanup(curl);
    curl_global_cleanup();

}


int main()
{
	//simpleCurl();	
	//# Step 1: Get request token
	GetRequestToken();
	parseToken(data);
	//# Step 2: Send user to the authorization page (we're not a web app, so we don't have a callback URL)
	//send user to : https://www.dropbox.com/1/oauth/authorize?oauth_token=%s for authorization
	GetAuthorize();
	string authUrl = "https://www.dropbox.com/1/oauth/authorize?oauth_token=" + tokenKey;
	output << "\nGo to following link to authorize this app. Then press 'a' \n" << authUrl << endl;
	while(1) {
		//cin.in();
		char a;
		cin >> a;
		if (a == 'a' || a == 'A') break;

	}

	//# Step 3: Get access token
	GetAccessToken();

	output.close();
	while(1)
	return 0;
}

