string data; //will hold the url's contents

size_t writeCallback(char* buf, size_t size, size_t nmemb, void* up)
{ //callback must have this declaration
    //buf is a pointer to the data that curl has for us
    //size*nmemb is the size of the buffer

    for (int c = 0; c<size*nmemb; c++)
    {
        data.push_back(buf[c]);
    }
    return size*nmemb; //tell curl how many bytes we handled
}

size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    size_t written;
    written = fwrite(ptr, size, nmemb, stream);
    return written;
}




void Prop( char *url, char *propData)
{
	CURL* curl; //curl handler
	struct curl_slist *slist=NULL;

	slist = curl_slist_append(slist, "Depth: 1");
	slist = curl_slist_append(slist, "Content-Type: text/xml; charset='UTF-8'");
	slist = curl_slist_append(slist, "User-Agent: DAVKit/4.0.1 (730); CalendarStore/4.0.1 (973); iCal/4.0.1 (1374); Mac OS X/10.6.2 (10C540)");

    curl_global_init(CURL_GLOBAL_ALL); //windows and ssl enabled


    curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_URL, url); //"https://p34-contacts.icloud.com/1260330658/carddavhome")
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
//	curl_easy_setopt(curl, CURLOPT_HEADER, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_easy_setopt(curl, CURLOPT_USERNAME, "siraj.zafl@gmail.com");
	curl_easy_setopt(curl, CURLOPT_PASSWORD, "JKoolip&123");
//	curl_easy_setopt(curl, CURLOPT_USERPWD, "siraj.zafl@gmail.com:JKoolip&123");
	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PROPFIND");
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, propData); //
	//curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "<ns0:propfind xmlns:ns0=\"DAV:\" xmlns:ns1=\"urn:ietf:params:xml:ns:carddav\"><ns0:prop><ns1:addressbook-home-set /></ns0:prop></ns0:propfind>");
	
	
	//curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "<A:propfind xmlns:A='DAV:'><A:prop><A:displayname/></A:prop></A:propfind>");
		
	//curl_easy_setopt(curl, CURLOPT_POSTFIELDS,"<c:calendar-query xmlns:d='DAV:' xmlns:c='urn:ietf:params:xml:ns:caldav'><d:prop><c:calendar-data /></d:prop><c:filter><c:comp-filter name='VCALENDAR'><C:comp-filter name='VEVENT'><C:time-range start='20141115T000000Z' end='20151201T000000Z'/></C:comp-filter></c:comp-filter></c:filter></c:calendar-query>");
	
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);
  //  curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); //tell curl to output its progress

    curl_easy_perform(curl);

    cout << endl << data << endl;
    cin.get();

    curl_easy_cleanup(curl);
    curl_global_cleanup();

}

void GetItem(char *url)
{
	CURL *curl;
    FILE *fp;
    CURLcode res;
    
    char outfilename[FILENAME_MAX] = "C:\\bbb.txt";
	curl_global_init(CURL_GLOBAL_ALL);
    curl = curl_easy_init();
    if (curl) {
        //fp = fopen(outfilename,"wb");
        curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_USERPWD, "siraj.zafl@gmail.com:JKoolip&123");
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	  //  curl_easy_setopt(curl, CURLOPT_ISSUERCERT,"curl-ca-bundle.cer");

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);
        //curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
		curl_global_cleanup();
        //fclose(fp);

		 cout << endl << data << endl;
		cin.get();
    }

}

void downloadContact()
{
	Prop("https://p20-caldav.icloud.com/", "<ns0:propfind xmlns:ns0=\"DAV:\"><ns0:prop><ns0:current-user-principal /></ns0:prop></ns0:propfind>");
	
	
	Prop("https://p20-contacts.icloud.com/1260330658/principal", "<ns0:propfind xmlns:ns0=\"DAV:\" xmlns:ns1=\"urn:ietf:params:xml:ns:carddav\"><ns0:prop><ns1:addressbook-home-set /></ns0:prop></ns0:propfind>");

}

int main()
{
 //  Prop("https://p20-contacts.icloud.com", "<A:propfind xmlns:A='DAV:'><A:prop><A:current-user-principal/></A:prop></A:propfind>");
//	Prop("https://p20-contacts.icloud.com/1260330658/principal", "<ns0:propfind xmlns:ns0=\"DAV:\" xmlns:ns1=\"urn:ietf:params:xml:ns:carddav\"><ns0:prop><ns1:addressbook-home-set /></ns0:prop></ns0:propfind>");
//	Prop("https://p34-contacts.icloud.com:443/1260330658/carddavhome","<ns0:propfind xmlns:ns0=\"DAV:\"><ns0:prop><ns0:resourcetype /></ns0:prop></ns0:propfind>");
	Prop("https://p01-contacts.icloud.com:443/1260330658/carddavhome/card","<ns0:propfind xmlns:ns0=\"DAV:\"><ns0:prop><ns0:resourcetype /></ns0:prop></ns0:propfind>");

	//GetItem("https://p20-contacts.icloud.com:443/1260330658/carddavhome/card/NzQzRUU0NDUtOTExMy00OTAxLTk1REYtODc1OTQ4QjMxMTc4.vcf");
//	GetItem("https://p20-contacts.icloud.com:443//1260330658/carddavhome/card/RjE1OUQ3NzAtQjBDNy00RkM5LUFBQ0YtOEQzMEI3RjI0RUU0.vcf");


	///******************************* CALENDARRRRRRR/

    // Prop("https://p20-caldav.icloud.com/", "<ns0:propfind xmlns:ns0=\"DAV:\"><ns0:prop><ns0:current-user-principal /></ns0:prop></ns0:propfind>");
	//Prop("https://p20-caldav.icloud.com/1260330658/calendars/", "<A:propfind xmlns:A='DAV:'><A:prop><A:displayname/></A:prop></A:propfind>");
	
	//Prop("https://p20-caldav.icloud.com/1260330658/calendars/20779686-D925-427F-851C-FFC0F4A1B306/","<ns0:propfind xmlns:ns0=\"DAV:\"><ns0:prop><ns0:resourcetype /></ns0:prop></ns0:propfind>");
	
	//Prop("https://p20-caldav.icloud.com/1260330658/calendars/20779686-D925-427F-851C-FFC0F4A1B306/","<A:propfind xmlns:A='DAV:'><A:prop><A:displayname/></A:prop></A:propfind>");
	//Prop("https://p20-caldav.icloud.com/1260330658/calendars/","<ns0:propfind xmlns:ns0=\"DAV:\" xmlns:ns1=\"urn:ietf:params:xml:ns:caldav\"><ns0:prop><ns1:displayname /></ns0:prop></ns0:propfind>");
	//Prop("https://p20-caldav.icloud.com/1260330658/calendars/", "<ns0:propfind xmlns:ns0=\"DAV:\"><ns0:allprop></ns0:allprop></ns0:propfind>");
    
	//GetItem("https://p20-caldav.icloud.com/1260330658/calendars/20779686-D925-427F-851C-FFC0F4A1B306/06BB096E-3B8F-4A25-A5E5-A84BFA2CA595.ics");

	return 0;
}





/*
int main()
{
    CURL* curl; //curl handler

    curl_global_init(CURL_GLOBAL_ALL); //windows and ssl enabled


    curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_URL, "http://www.google.com/");//http://www.google.com/
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);
  //  curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); //tell curl to output its progress

    curl_easy_perform(curl);

    cout << endl << data << endl;
    cin.get();

    curl_easy_cleanup(curl);
    curl_global_cleanup();

    return 0;
}
*/