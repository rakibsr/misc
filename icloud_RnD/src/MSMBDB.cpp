#include "MSMBDB.h"


MSMBDB::MSMBDB(int snapshotId, MBDB mbdb)
{
	set_snapshotId(snapshotId);
	set_mbdb(mbdb);
}


MSMBDB::~MSMBDB(void)
{
}


int MSMBDB::fetch_snapshotId() {
		return _snapshotId;
	}


 void MSMBDB::set_snapshotId(int _snapshotId) {
		this->_snapshotId = _snapshotId;
	}


 MBDB MSMBDB::fetch_mbdb() {
		return _mbdb;
	}


 void MSMBDB::set_mbdb(MBDB _mbdb) {
		this->_mbdb = _mbdb;
	}
	
