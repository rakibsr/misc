#pragma once
#include "DavAccessFactory.h"
#include "BackupDefinition.pb.h"

#include <cstring>
#include <vector>
#include <map>
#include <cstdint>

using namespace BackupProtoGen;
 class BackupDavFactory : public DavAccessFactory
{

private :
	
	//static final string TAG = UnityConstants.TAGPREFIX + BackupDavFactory.class.getSimpleName();
	
	map< int, SnapshotDefinition > _snapshotDefinitions;

	 bool mSessionOpened;

	

	// DownloadProgressInterface mDownloadProgressCallback = null;
	// StatusProgressInterface mStatusCallback = null;

	 int mCurrType;

	 long mThrottle;
	 long mLastDownloadProgressUpdateTime;

  //   Context _context = null;

	  long mRunningBandwidth ;

public:

	std::string _userID;
	string _appleID;
	string _password;
	string _mmeAuthToken;
	string _secondMmeAuthToken; 
	string _mmeFMIPToken;
	string _host;

	string _dsid;

	int _dsPrsID;
	int  _backupID;


	vector<uint8_t> _getFileData;
	vector<uint8_t> _fileTokenAsData;

	string _fileToken;

	string _protocolVersionAcct;
	string _protocolVersionAuth;


	string _quotaInfoURL; 
	string _quotaUpdateURL; 	  
	string _X_Request_Origin; 

	string _backupServerURL;
    string _contentServerURL;
	string _backupFirstEntryID;

	vector<string> _appSet ;

    string _appArray;
    string _jsonPhotostring;


	//vector<Device> _deviceList = new ArrayList<Device>();

	vector<string> _deviceID;
	//public DavFactoryData data = new DavFactoryData();

	 string principle;
	 string home_set;
	 string groupsList;
	 string itemURLList;
	 string items;

	
	  long mTotalDownloadedFileSize;
	  long mMaxFileSize; //maximum file size for the current download

	  int mNetworkConnectivityRetryTime;  // in seconds

	
	//for calculate running bandwidth
	  long mStartTime ;
	  long mTempByteSize ;



	BackupDavFactory(void);
	~BackupDavFactory(void);
};

