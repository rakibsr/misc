#pragma once

#include "MBDB.pb.h"

using namespace BackupProtoGen;
class MSMBDB
{
private :
	 int _snapshotId ;
	 MBDB _mbdb;
public:
	MSMBDB(int snapshotId, MBDB mbdb);
	~MSMBDB(void);

	int fetch_snapshotId();
	void set_snapshotId(int _snapshotId);
	MBDB fetch_mbdb();
	void set_mbdb(MBDB _mbdb);
};

