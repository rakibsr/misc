﻿

/* Created in 2015/2016.
Investigation on getting data from icloud.
This investigation was done for developing "Smart Switch Mobile (Tizen app)
This is just a scratch(rough) coding.
Author: rakibdana@gmail.com/rakibsr@gmail.com
*/

#include<stdio.h>
#include<dos.h>
#include<stdlib.h>
#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<numeric>
#include<vector>
#include<cstring>
#include<map>
#include<iterator>

#include <iostream>
#include <fstream>
#include <string>
#include "base64.h"
#include <curl.h> //your directory may be different


#include "BackupDefinition.pb.h"
#include "MBDB.pb.h"
#include "protoDecoder.c"
#include "charsetConversion.h"


#include <google/protobuf/text_format.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/message.h>
#include <fcntl.h>

using namespace std;
using namespace BackupProtoGen;

string data; //will hold the url's contents
vector<char> bytes;

//fileId: KlvpHEuzoqGfLrkKdFguFgAShGM=,Domain: CameraRollDomain, Path: Media/DCIM/100APPLE/IMG_0020.JPG, FileUUId: AT/vfVUCrzuLa9akqxGwOlReaBcf, fileSize: 118880, Wrapped key: , unknown1: 375985, unix_i_mode: 33188, userId1: 501, userId2: 501, mtime: 1423137179, atime: 1426956025, ctime: 1423137178, protectionClass: 3, key: com.apple.cpl.original, value: 1, key: com.apple.assetsd.customCreationDate, value: 50, key: com.apple.assetsd.UUID, value: 16, key: com.apple.assetsd.assetType, value: 2, key: com.apple.assetsd.dbRebuildUuid, value: 36, key: com.apple.assetsd.avalanche.type, value: 2, key: com.apple.assetsd.favorite, value: 2, key: com.apple.assetsd.cloudAsset.UUID, value: 36, key: com.apple.assetsd.addedDate, value: 50, key: com.apple.assetsd.hidden, value: 2, key: com.apple.assetsd.trashed, value: 2
//token = BQXDHTaYDfyeA1FOKg==
string token ="BQXDHTaYDfyeA1FOKg==";
string fileId = "KlvpHEuzoqGfLrkKdFguFgAShGM=";
int filesize =118880;
string FileUUId= "AT/vfVUCrzuLa9akqxGwOlReaBcf";
string uri = "/Sfmez-UBTD1A0O4AxNnB?x-client-request-id=a40a5d8d-0932-42e4-92d1-0ab2552801e5&Expires=1428659752&byte-range=779680-6444951&AWSAccessKeyId=AKIAIWWR33ECHKPC2LUA&Signature=4cDmujRZ8DFCEi6uZpbQ2ERDyJY%3D";
string storage_container_authorization_token = "A8DAb0BcdXtZtU4q";
string storage_container_key ="Sfmez-UBTD1A0O4AxNnB";
string hostname="us-std-00001.s3-external-1.amazonaws.com";
string chunk_checksum="gQgpj+xxp43ZbvV5HrQGrztcz7oF";
string chunk_encryption_key="AeGgxrZdj6RPbe1yPLQTgSA=";

string imgURL = "http://images.nationalgeographic.com/wpf/media-live/photos/000/007/cache/siberian-tiger_707_600x450.jpg";

size_t writeCallbackBytes(char* buf, size_t size, size_t nmemb, void* up)
{ 
	bytes.clear();
	bytes.insert(bytes.end(),buf,buf+size*nmemb);// copy bytes to vector
    return size*nmemb; //tell curl how many bytes we handled
}

size_t writeCallback(char* buf, size_t size, size_t nmemb, void* up)
{ 
    for (int c = 0; c<size*nmemb; c++)
    {
        data.push_back(buf[c]);
    }
    return size*nmemb; //tell curl how many bytes we handled
}

void simpleCurl()
{
	CURL* curl; //curl handler

    curl_global_init(CURL_GLOBAL_ALL); //windows and ssl enabled


    curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_URL, imgURL.c_str());//http://www.google.com/
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);
  //  curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); //tell curl to output its progress

    curl_easy_perform(curl);

	fstream output("out.jpg", ios::out | ios::binary);
	output << data ;
	output.close();
  //  cout << endl << data << endl;

    curl_easy_cleanup(curl);
    curl_global_cleanup();
}

void fetchAuth()
{
	char *url = "https://setup.icloud.com/setup/authenticate/$APPLE_ID$"; //+_appleID;

	CURL* curl; //curl handler
	struct curl_slist *slist=NULL;

	curl_global_init(CURL_GLOBAL_ALL); //windows and ssl enabled
	curl = curl_easy_init();

	/*
	
		request.addHeader("User-Agent","iCloud.exe (unknown version) CFNetwork/520.2.6!");
		request.addHeader("X-Mme-Client-Info","<iPod4,1> <iPhone OS;5.0.1;9A405> <com.apple.AppleAccount/1.0 (com.apple.Preferences/1.0)>");
		request.addHeader("Authorization", "Basic "+Base64.encodeToString(new String(_appleID+":"+_password).getBytes(),Base64.NO_WRAP));
		request.addHeader("Host","setup.icloud.com");
	*/

	slist = curl_slist_append(slist, "User-Agent: iCloud.exe (unknown version) CFNetwork/520.2.6!");
	slist = curl_slist_append(slist, "X-Mme-Client-Info: <iPod4,1> <iPhone OS;5.0.1;9A405> <com.apple.AppleAccount/1.0 (com.apple.Preferences/1.0)>");
	
	string uNamPW = "mehedi@dhrubokinfotech.com:Loopy1123";//"siraj.zafl@gmail.com:JKoolip&123";
	string authBasic= "Authorization: Basic "+ base64_encode(reinterpret_cast<const unsigned char*>(uNamPW.c_str()), uNamPW.length());
	slist = curl_slist_append(slist, authBasic.c_str());
	slist = curl_slist_append(slist, "Host: setup.icloud.com");

    curl_easy_setopt(curl, CURLOPT_URL, url); 
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);

	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	//curl_easy_setopt(curl, CURLOPT_USERNAME, "siraj.zafl@gmail.com");
	//curl_easy_setopt(curl, CURLOPT_PASSWORD, "JKoolip&123");
	curl_easy_setopt(curl, CURLOPT_USERPWD, "mehedi@dhrubokinfotech.com:Loopy1123");
	curl_easy_setopt(curl, CURLOPT_PORT,443); 
	curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);

    curl_easy_perform(curl);

	//freopen("out.txt", "w+", stdout);
	
	fstream output("out.txt", ios::out | ios::binary);
	output << data ;
	output.close();

    curl_easy_cleanup(curl);
    curl_global_cleanup();

}

// output of fetchAuth(). This is set from getAuthDictionary()
string _protocolVersionAuth = "1.0";
string _dsid="543445470";//"1260330658"->siraj;
string _dsPrsID="543445470";//"1260330658";
string _mmeAuthToken="AQAAAABVJPg/fFGuk1WG0rU6z5uxGRRGc1/lvhQ=";//"AQAAAABVBy2ov0g74En4zVEkhCQAP10RZDoNCx4="; //always change

void fetchAccountSettings()
{
	char *url = "https://setup.icloud.com/setup/get_account_settings";

	CURL* curl; //curl handler
	struct curl_slist *slist=NULL;

	curl_global_init(CURL_GLOBAL_ALL); //windows and ssl enabled
	curl = curl_easy_init();

	/*
  		request.addHeader("User-Agent","iCloud.exe (unknown version) CFNetwork/520.2.6!");
		request.addHeader("X-Mme-Client-Info","<iPod4,1> <iPhone OS;5.0.1;9A405> <com.apple.AppleAccount/1.0 (com.apple.Preferences/1.0)>");
		request.addHeader("Authorization", "Basic "+Base64.encodeToString(new String(_dsPrsID+":"+_mmeAuthToken).getBytes(),Base64.NO_WRAP));
		request.addHeader("Host","setup.icloud.com");
	*/

	slist = curl_slist_append(slist, "User-Agent: iCloud.exe (unknown version) CFNetwork/520.2.6!");
	slist = curl_slist_append(slist, "X-Mme-Client-Info: <iPod4,1> <iPhone OS;5.0.1;9A405> <com.apple.AppleAccount/1.0 (com.apple.Preferences/1.0)>");
	
	string uNamPW = _dsPrsID+":"+_mmeAuthToken; //change is here
	string authBasic= "Authorization: Basic "+ base64_encode(reinterpret_cast<const unsigned char*>(uNamPW.c_str()), uNamPW.length());
	slist = curl_slist_append(slist, authBasic.c_str());
	slist = curl_slist_append(slist, "Host: setup.icloud.com");

    curl_easy_setopt(curl, CURLOPT_URL, url); 
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);

	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	//curl_easy_setopt(curl, CURLOPT_USERNAME, "siraj.zafl@gmail.com");
	//curl_easy_setopt(curl, CURLOPT_PASSWORD, "JKoolip&123");
	curl_easy_setopt(curl, CURLOPT_USERPWD, "mehedi@dhrubokinfotech.com:Loopy1123");
	curl_easy_setopt(curl, CURLOPT_PORT,443); 
	curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);

    curl_easy_perform(curl);

	//freopen("out.txt", "w+", stdout);
	
		fstream output("out.txt", ios::out | ios::binary);

		output << data ;
		output.close();
   // cin.get();

    curl_easy_cleanup(curl);
    curl_global_cleanup();

}


//Output of fetchAccountSettings(). These are set in getAccountsDictionary()
string _protocolVersionAcct ="2";
string _quotaInfoURL="https://p29-quota.icloud.com:443/quotaservice/external/ios/543445470/getQuotaInfo";// check it dsprsid
string _quotaUpdateURL="https://p29-quota.icloud.com:443/quotaclient/";
string _X_Request_Origin="https://p29-quota.icloud.com:443/quotastatic/desktop/";
string _backupServerURL="https://p29-mobilebackup.icloud.com:443";//p34-
string _contentServerURL="https://p29-content.icloud.com:443";
string _secondMmeAuthToken="AQAAAABVJPg/fFGuk1WG0rU6z5uxGRRGc1/lvhQ=";//"AQAAAABVBy2ov0g74En4zVEkhCQAP10RZDoNCx4="; //chinaging thing
string _mmeFMIPToken="AQAAAABVJPjgwOkZHoqLMywhhKYWCTjpVBHR69k~";//"AQAAAABVBzBsqpt7HclH3zpbyOlAsjs-sfXidaM~"; //chinaging thing
string _host="p29-setup.icloud.com:443";//"p34-setup.icloud.com:443"; // Never put 'https:\\' on host. 

void fetchQuotaDetails()
{
	char *url = "https://p29-quota.icloud.com:443/quotaservice/external/ios/543445470/getQuotaDetails"; //_quotaInfoURL.replace("Info", "Details");

	CURL* curl; //curl handler
	struct curl_slist *slist=NULL;

	curl_global_init(CURL_GLOBAL_ALL); //windows and ssl enabled
	curl = curl_easy_init();

	/*
  		request.addHeader("User-Agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/534.57.7 (KHTML, like Gecko) iCloudSettings/1.0");
		request.addHeader("X-Mme-Client-Info","<iPod4,1> <iPhone OS;5.0.1;9A405> <com.apple.AppleAccount/1.0 (com.apple.Preferences/1.0)>");
		request.addHeader("Authorization", "Basic "+Base64.encodeToString(new String(_dsPrsID+":"+_secondMmeAuthToken).getBytes(),Base64.NO_WRAP));
		request.addHeader("X-Request-Origin", _X_Request_Origin);
		request.addHeader("Origin","www.icloud.com");
		request.addHeader("Referer","www.icloud.com");
		request.addHeader("Host",_host);
		request.addHeader("Content-type","application/json");

	%%%%%% From android log : %%%%%%%%%
		: https://p34-quota.icloud.com:443/quotaservice/external/ios/1260330658/getQuotaDetails, 
		: https://p34-quota.icloud.com:443/quotaservice/external/ios/1260330658/getQuotaDetails HTTP/1.1
		: User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/534.57.7 (KHTML, like Gecko) iCloudSettings/1.0  
		: X-Mme-Client-Info: <iPod4,1> <iPhone OS;5.0.1;9A405> <com.apple.AppleAccount/1.0 (com.apple.Preferences/1.0)>  
		: Authorization: Basic MTI2MDMzMDY1ODpBUUFBQUFCVkIxaDVtOVFBSzYyWEwwcVNBNmExVGNFVjVTU1FoMFU9  
		: X-Request-Origin: https://p34-quota.icloud.com:443/quotastatic/desktop/  
		: Origin: www.icloud.com  
		: Referer: www.icloud.com  
		: Host: p34-setup.icloud.com:443  
		: Content-type: application/json  
	*/

	slist = curl_slist_append(slist, "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/534.57.7 (KHTML, like Gecko) iCloudSettings/1.0");//changed
	slist = curl_slist_append(slist, "X-Mme-Client-Info: <iPod4,1> <iPhone OS;5.0.1;9A405> <com.apple.AppleAccount/1.0 (com.apple.Preferences/1.0)>");
	
	string secMme = _dsPrsID+":"+_secondMmeAuthToken; //again change is here
	string authBasic= "Authorization: Basic "+ base64_encode(reinterpret_cast<const unsigned char*>(secMme.c_str()), secMme.length());
	slist = curl_slist_append(slist, authBasic.c_str());
	cout<<authBasic;
//	slist=curl_slist_append(slist,"Authorization: Basic MTI2MDMzMDY1ODpBUUFBQUFCVkIxaDVtOVFBSzYyWEwwcVNBNmExVGNFVjVTU1FoMFU9");

	string xreq="X-Request-Origin:"+_X_Request_Origin;
	slist = curl_slist_append(slist, xreq.c_str());

	slist = curl_slist_append(slist,"Origin: www.icloud.com");
	slist = curl_slist_append(slist,"Referer: www.icloud.com");

	string host = "Host: "+_host;
	slist = curl_slist_append(slist, host.c_str()); // Never put 'https:\\' on host. 

	slist = curl_slist_append(slist,"Content-type: application/json");

    curl_easy_setopt(curl, CURLOPT_URL, url); 
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);

	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
//	curl_easy_setopt(curl, CURLOPT_USERNAME, "siraj.zafl@gmail.com");
//	curl_easy_setopt(curl, CURLOPT_PASSWORD, "JKoolip&123");
	curl_easy_setopt(curl, CURLOPT_USERPWD, "mehedi@dhrubokinfotech.com:Loopy1123");
	curl_easy_setopt(curl, CURLOPT_PORT,443); 
	curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);

    curl_easy_perform(curl);

	//freopen("out.txt", "w+", stdout);
	
		fstream output("out.txt", ios::out | ios::binary);

		output << data ;
		output.close();
  //  cin.get();

    curl_easy_cleanup(curl);
    curl_global_cleanup();

}


// ** Output of fetchQuotaDetails(). These are set in getBackupInfo()
//JSON parsing needed
string _deviceName= "iPhone";//"iPhone-6";
string _deviceID = "389673d4e9fbfbb546b548bbc381d61724b87cb6";//"04bbd30a43b99cb04669a9e4d42296c3e3602e38";
string _backupFirstEntryID = "389673d4e9fbfbb546b548bbc381d61724b87cb6";//"04bbd30a43b99cb04669a9e4d42296c3e3602e38";
string _productType= "iPhone4,1";//"iPhone7,1";
string _encodedKey = "eyJpZCI6IjM4OTY3M2Q0ZTlmYmZiYjU0NmI1NDhiYmMzODFkNjE3MjRiODdjYjYiLCJ0eXBlIjoiYmFja3VwIn0%3D";//"eyJpZCI6IjA0YmJkMzBhNDNiOTljYjA0NjY5YTllNGQ0MjI5NmMzZTM2MDJlMzgiLCJ0eXBlIjoiYmFja3VwIn0%3D";
string _deviceUdid= "389673d4e9fbfbb546b548bbc381d61724b87cb6";//"04bbd30a43b99cb04669a9e4d42296c3e3602e38";
string _storageUsedInBytes = "18689982";
string _lastModified= "1426957181000";//"1426477943000";

//string  _backupServerURL ="https://p34-mobilebackup.icloud.com:443";
//string  _dsPrsID = "1260330658";
//string  _backupFirstEntryID = "04bbd30a43b99cb04669a9e4d42296c3e3602e38";
//string  _secondMmeAuthToken = "AQAAAABU/vR3xvYm6HE3/bBBe1+KqJuYWwNjcRc=";

void enumerateSnapshot()
{
	CURL *curl;
    CURLcode res;
	struct curl_slist *slist=NULL;

	string url = _backupServerURL+"/mbs/"+_dsPrsID+"/"+_backupFirstEntryID;

	//slist = curl_slist_append(slist, "Depth: 1");
	curl_global_init(CURL_GLOBAL_ALL);

    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str()); 
		//curl_easy_setopt(curl, CURLOPT_USERPWD, "siraj.zafl@gmail.com:JKoolip&123");
		curl_easy_setopt(curl, CURLOPT_USERPWD, "mehedi@dhrubokinfotech.com:Loopy1123");
		curl_easy_setopt(curl, CURLOPT_PORT,443); 
		curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	//	curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING,""); //not supported in this version of curl

		slist = curl_slist_append(slist, "User-Agent: Backup/6.1.3 (10B329; iPhone3,1");
		string secMme = _dsPrsID+":"+_secondMmeAuthToken; //same as fetchQuotaDetails()
		string authBasic= "Authorization: Basic "+ base64_encode(reinterpret_cast<const unsigned char*>(secMme.c_str()), secMme.length());
		slist = curl_slist_append(slist, authBasic.c_str());

		slist = curl_slist_append(slist,"X-Mme-Client-Info: <iPhone3,1> <iPhone OS;6.1.3;10B329> <com.apple.AppleAccount/1.0 (com.apple.backupd/(null))>");
		slist = curl_slist_append(slist,"Host: p05-mobilebackup.icloud.com");
        slist = curl_slist_append(slist,"Accept: application/vnd.com.apple.mbs+protobuf");
        slist = curl_slist_append(slist,"X-Apple-MBS-Protocol-Version: 1.7");
		slist = curl_slist_append(slist,"Accept-Encoding: charset=ISO-8859-1"); // not sure now

		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);

		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);

        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
		curl_global_cleanup();

		fstream output("out.txt", ios::out | ios::binary);
		output << data ;

		/*
		fstream output("out.txt", ios::out | ios::binary);
		for(int i=0;i<bytes.size();i++)
		{
			output << bytes[i] ;
		}
		*/
		output.close();
    }
}

//Following data found enumerateSnapshot().Following data set in getSnapshotIndices( responseBytes )as -
//we need proto buffer parsing
int _snapshotIndices[1]={1};//int _snapshotIndices[3]={1,9,10};

string snapshot_id ="1";//int
int date1 = 1426656655;
int date2 = 1426141609;
string device_definition_unknown2="2";//int
string device_definition_unknown3 ="MTeQ0UrgTU6+XDpiVLt+lA==";
int device_definition_unknown4=2;
int device_definition_unknown5=1;

string kFilesLimit = "1000"; //hardcoded in code

void fetchSnapshotSegment()
{
	string snapshotnumber=snapshot_id;
	string offset="0";//int
	
	CURL *curl;
    CURLcode res;
	struct curl_slist *slist=NULL;

	string url = _backupServerURL+"/mbs/"+_dsPrsID+"/"+_backupFirstEntryID+"/"+snapshotnumber+"/listFiles?offset="+offset+"&limit=" + kFilesLimit;

	//slist = curl_slist_append(slist, "Depth: 1");
	curl_global_init(CURL_GLOBAL_ALL);

    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str()); 
		curl_easy_setopt(curl, CURLOPT_USERPWD, "mehedi@dhrubokinfotech.com:Loopy1123");
	//	curl_easy_setopt(curl, CURLOPT_USERPWD, "siraj.zafl@gmail.com:JKoolip&123");
		curl_easy_setopt(curl, CURLOPT_PORT,443); 
		curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	//	curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING,""); //not supported in this version of curl

		slist = curl_slist_append(slist, "User-Agent: Backup/6.1.3 (10B329; iPhone3,1)");
		string secMme = _dsPrsID+":"+_secondMmeAuthToken; //same as fetchQuotaDetails(),enumerateSnapshot()
		string authBasic= "Authorization: Basic "+ base64_encode(reinterpret_cast<const unsigned char*>(secMme.c_str()), secMme.length());
		slist = curl_slist_append(slist, authBasic.c_str());

		slist = curl_slist_append(slist,"X-Mme-Client-Info: <iPhone3,1> <iPhone OS;6.1.3;10B329> <com.apple.AppleAccount/1.0 (com.apple.backupd/(null))>");
		slist = curl_slist_append(slist,"Host: p05-mobilebackup.icloud.com");
        slist = curl_slist_append(slist,"Accept: application/vnd.com.apple.mbs+protobuf");
        slist = curl_slist_append(slist,"X-Apple-MBS-Protocol-Version: 1.7");
		slist = curl_slist_append(slist,"Accept-Encoding: charset=ISO-8859-1"); // not sure now

		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);

		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);

        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
		curl_global_cleanup();

	//	freopen("out.txt", "w+", stdout);
	//	cout << endl << data << endl;

		fstream output("out.txt", ios::out | ios::binary);
		output << data ;
		/*
		fstream output("out.txt", ios::out | ios::binary);
		for(int i=0;i<bytes.size();i++)
		{
			output << bytes[i] ;
		}
		*/
		output.close();
    }
}

void prefetchAuthForFiles()
{
	string theSnapID="1";
	string offset="0";//int
	
	CURL *curl;
    CURLcode res;
	struct curl_slist *slist=NULL;

	char filesPayload[] = {0x16,0x14,0x2a,0x5b,0xe9,0x1c,0x4b,0xb3,0xa2,0xa1,0x9f,0x2e,0xb9,0x74,0x58,0x2e,0x16,0x00,0x12,0x84,0x63,0x16,0x14,0x8b,0xc4,0xa3,0xf9,0xa9,0x1b,0xe6,0x4b,0x7e,0xde,0x88,0x02,0x65,0x57,0x72,0xc3,0x1b,0xf8,0x18,0xbc,0x16,0x14,0xc9,0xcf,0x4a,0x1e,0xee,0xdf,0x55,0x5e,0x32,0x38,0x72,0x4d,0xa0,0x9a,0xe8,0xa5,0x4e,0xda,0xcd,0x73,0x16,0x14,0xd0,0x5b,0xe5,0x10,0x65,0x52,0x55,0x60,0x36,0xcd,0x29,0xc6,0xdb,0xc6,0x64,0x67,0x41,0xe0,0xa8,0xa6,0x16,0x14,0xf1,0x27,0x00,0xdc,0x87,0xd7,0xe9,0x79,0x1a,0x33,0xfd,0xf1,0x09,0x63,0x80,0xd7,0xb8,0x11,0x67,0x6e};
	//string f="FgoUKlvpHEuzoqGfLrkKdFguFgAShGMWChSLxKP5qRvmS37eiAJlV3LDG/gYvBYKFMnPSh7u31VeMjhyTaCa6KVO2s1zFgoU0FvlEGVSVWA2zSnG28ZkZ0HgqKYWChTxJwDch9fpeRoz/fEJY4DXuBFnbg==";
	char* baseEn ="S2x2RHFSeEx3clBDb3NLaHdwOHV3cmwwV0M0Vzc3Kzk3Nys5RXNLRVl4WVV3b3ZEaE1Lanc3bkNxUnZEcGt0K3c1N0NpQUpsVjNMRGd4dkR1QmpDdkJZVXc0bkRqMG9ldzY3RG4xVmVNamh5VGNLZ3dwckRxTUtsVHNPYXc0MXpGaFREa0Z2RHBSQmxVbFZnTnNPTktjT0d3NXZEaG1SblFjT2d3cWpDcGhZVXc3RW43Nys5NzcrOXc1ekNoOE9YdzZsNUdqUER2Y094Q1dQQ2dNT1h3cmdSWjI0PQ==";

	byte b[]={22, 20, 42, 91, -61, -87, 28, 75, -62, -77, -62, -94, -62, -95, -62, -97, 46, -62, -71, 116, 88, 46, 22, 0, 18, -62, -124, 99, 22, 20, -62, -117, -61, -124, -62, -93, -61, -71, -62, -87, 27, -61, -90, 75, 126, -61, -98, -62, -120, 2, 101, 87, 114, -61, -125, 27, -61, -72, 24, -62, -68, 22, 20, -61, -119, -61, -113, 74, 30, -61, -82, -61, -97, 85, 94, 50, 56, 114, 77, -62, -96, -62, -102, -61, -88, -62, -91, 78, -61, -102, -61, -115, 115, 22, 20, -61, -112, 91, -61, -91, 16, 101, 82, 85, 96, 54, -61, -115, 41, -61, -122, -61, -101, -61, -122, 100, 103, 65, -61, -96, -62, -88, -62, -90, 22, 20, -61, -79, 39, 0, -61, -100, -62, -121, -61, -105, -61, -87, 121, 26, 51, -61, -67, -61, -79, 9, 99, -62, -128, -61, -105, -62, -72, 17, 103, 110};
	string url ="https://p29-mobilebackup.icloud.com:443/mbs/543445470/389673d4e9fbfbb546b548bbc381d61724b87cb6/1/getFiles"; 
	//_backupServerURL+"/mbs/"+_dsPrsID+"/"+_backupFirstEntryID+"/"+theSnapID+"/getFiles";
	curl_global_init(CURL_GLOBAL_ALL);

    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str()); 
		curl_easy_setopt(curl, CURLOPT_USERPWD, "mehedi@dhrubokinfotech.com:Loopy1123");//mehedi@dhrubokinfotech.com:Loopy1123
		curl_easy_setopt(curl, CURLOPT_PORT,443); 
		curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	//	curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING,""); //not supported in this version of curl

		slist = curl_slist_append(slist, "User-Agent: Backup/6.1.3 (10B329; iPhone3,1)");
		string secMme = _dsPrsID+":"+_secondMmeAuthToken; //same as fetchQuotaDetails(),enumerateSnapshot()
		string authBasic= "Authorization: Basic NTQzNDQ1NDcwOkFRQUFBQUJWSjA1SmVCYkc1eEp4bStudVdyVmt4ZGRkUXdxSkVRVT0=";
		//"Authorization: Basic "+ base64_encode(reinterpret_cast<const unsigned char*>(secMme.c_str()), secMme.length());
		cout<<authBasic;
		slist = curl_slist_append(slist, authBasic.c_str());

		slist = curl_slist_append(slist,"X-Mme-Client-Info: <iPhone3,1> <iPhone OS;6.1.3;10B329> <com.apple.AppleAccount/1.0 (com.apple.backupd/(null))>");
		slist = curl_slist_append(slist,"Host: https://p29-mobilebackup.icloud.com:443");//Host: p05-mobilebackup.icloud.com
        slist = curl_slist_append(slist,"Accept: application/vnd.com.apple.mbs+protobuf");
        slist = curl_slist_append(slist,"X-Apple-MBS-Protocol-Version: 1.7");
		slist = curl_slist_append(slist,"Content-type:application/vnd.com.apple.mbs+protobuf");
		slist = curl_slist_append(slist,"Accept-Encoding: charset=ISO-8859-1"); // not sure now

		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);

		curl_easy_setopt(curl,  CURLOPT_POST, 1);
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_easy_setopt(curl,  CURLOPT_POSTFIELDS, filesPayload);

		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);

        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
		curl_global_cleanup();

	//	freopen("out.txt", "w+", stdout);
		cout << data <<endl ;
		cout<<res;
    }
}


void Prop( char *url, string propData)
{
	CURL* curl; //curl handler
	struct curl_slist *slist=NULL;

	slist = curl_slist_append(slist, "Depth: 1");
	slist = curl_slist_append(slist, "Content-Type: text/xml; charset='UTF-8'");
//	slist = curl_slist_append(slist, "User-Agent:  iCloud.exe (unknown version) CFNetwork/520.2.6");
//	slist = curl_slist_append(slist, "X-Mme-Client-Info: <PC> <Windows; 6.1.7601/SP1.0; W> <com.apple.AOSKit/88>");
//	slist = curl_slist_append(slist, "Accept-Language: en-US");
//	slist = curl_slist_append(slist, "Authorization: Basic");

    curl_global_init(CURL_GLOBAL_ALL); //windows and ssl enabled

    curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_URL, url); //"https://p34-contacts.icloud.com/1260330658/carddavhome")
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
	curl_easy_setopt(curl, CURLOPT_HEADER, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);

	curl_easy_setopt(curl, CURLOPT_USERPWD, "siraj.zafl@gmail.com:JKoolip&123");
	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PROPFIND");
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, propData.c_str()); //
			
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);

    curl_easy_perform(curl);

    cout << endl << data << endl;
    cin.get();

    curl_easy_cleanup(curl);
    curl_global_cleanup();

}

void GetItem(char *url)
{
	CURL *curl;
    CURLcode res;
    
	curl_global_init(CURL_GLOBAL_ALL);
    curl = curl_easy_init();
    if (curl) 
	{
        curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_USERPWD, "siraj.zafl@gmail.com:JKoolip&123");

		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	  //  curl_easy_setopt(curl, CURLOPT_ISSUERCERT,"curl-ca-bundle.cer");

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);
     
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
		curl_global_cleanup();

		cout << endl << data << endl;
		cin.get();
    }

}

int main()
{
//	simpleCurl();	
	
//	fetchAuth();
//	fetchAccountSettings();
//	fetchQuotaDetails();
//	enumerateSnapshot();
//	fetchSnapshotSegment();
  prefetchAuthForFiles();



//	freopen("parsed.txt", "w+", stdout);	
	//PROTO for BackupDefinition >> enumerateSnapshot();

	/*
	GOOGLE_PROTOBUF_VERIFY_VERSION;
    com::markspace::webdav::BackupProtoGen::BackupDefinition bd;	 
  //  fstream input("out.txt", ios::in | ios::binary);
	if(!bd.ParseFromString(data))    //if(!bd.ParseFromIstream(&input))
	{
		 cerr << "Failed to PARSE." ;
	} 
	else
	{
		int snapshotSize = bd.snapshots_size();
		for(int i=0;i<snapshotSize;i++)
		{
			cout<<bd.snapshots(i).snapshot_id()<<endl;
		}
	}
	google::protobuf::ShutdownProtobufLibrary();
	*/
	   
	//char filesPayload[] = {0x16, 0x0a, 0x14, 0x2a, 0x5b ,0xe9 ,0x1c ,0x4b ,0xb3 ,0xa2 ,0xa1 ,0x9f ,0x2e ,0xb9 ,0x0a ,0x74 ,0x58 ,0x2e ,0x16 ,0x00 ,0x12 ,0x84 ,0x63 ,0x16 ,0x0a ,0x14 ,0x8b ,0xc4 ,0xa3 ,0xf9 ,0xa9 ,0x1b ,0xe6 ,0x4b ,0x7e ,0xde ,0x88 ,0x02 ,0x65 ,0x57 ,0x72 ,0xc3 ,0x1b ,0xf8 ,0x18 ,0xbc ,0x16 ,0x0a ,0x14 ,0xc9 ,0xcf ,0x4a ,0x1e ,0xee ,0xdf ,0x55 ,0x5e ,0x32 ,0x38 ,0x72 ,0x4d ,0xa0 ,0x9a ,0xe8 ,0xa5 ,0x4e ,0xda ,0xcd ,0x73 ,0x16 ,0x0a ,0x14 ,0xd0 ,0x5b ,0xe5 ,0x10 ,0x65 ,0x52 ,0x55 ,0x60 ,0x36 ,0xcd ,0x29 ,0xc6 ,0xdb ,0xc6 ,0x64 ,0x67 ,0x41 ,0xe0 ,0xa8 ,0xa6 ,0x16 ,0x0a ,0x14 ,0xf1 ,0x27 ,0x00 ,0xdc ,0x87 ,0xd7 ,0xe9 ,0x79 ,0x1a ,0x33 ,0xfd ,0xf1 ,0x09 ,0x63 ,0x80 ,0xd7 ,0xb8 ,0x11 ,0x67 ,0x6e};

	/*
	  //PROTO for prefetch
	 GOOGLE_PROTOBUF_VERIFY_VERSION; 
	 int cnt=0;
	 while(1)
	 {
		 if (cnt >= data.length())
		 {
			 cout<<"\nBreaking...When cnt = "<<cnt<<" and data len = "<<data.length();
			 break;
		 }

		int len = data[ cnt ]; //first charater
        if (len < 0) len += 256; 
        cnt++;

        int lenOffset = (int)data[ cnt ]; //next character        
        if( lenOffset < 0 ) lenOffset += 256;

        if( (lenOffset > 0 && lenOffset < 8) || lenOffset>10)  //rd. previous :   if( lenOffset > 0 && lenOffset < 8 )
        {
            lenOffset--;
            len += (lenOffset * 128 );
            cnt++;
        }

		string sub = data.substr(cnt,len);
       // com::markspace::webdav::BackupProtoGen::MBDB mbdb;	
		MBDB mbdb;

        if(!mbdb.ParseFromString(sub))
		{
			cout << "Failed to PARSE. :((" ;
			break;
        }
		else
		{
			cout<<"\nfileId: "<<base64_encode(reinterpret_cast<const unsigned char*>(mbdb.fileid().c_str()),mbdb.fileid().length())
				<<",Domain: " <<mbdb.domain()
				<<", Path: "<<mbdb.path()
				<<", FileUUId: "<<base64_encode(reinterpret_cast<const unsigned char*>(mbdb.fileuuid().c_str()),mbdb.fileuuid().length())
				<<", fileSize: "<<mbdb.filesize();

			if(mbdb.has_properties())
			{
				cout<<", Wrapped key: "<<base64_encode(reinterpret_cast<const unsigned char*>(mbdb.properties().wrappedkey().c_str()),mbdb.properties().wrappedkey().length())			
					<<", unknown1: "<<mbdb.properties().unknown1()
					<<", unix_i_mode: "<<mbdb.properties().unix_i_mode()
					<<", userId1: "<<mbdb.properties().userid1()
					<<", userId2: "<<mbdb.properties().userid2()
					<<", mtime: "<<mbdb.properties().mtime()
					<<", atime: "<<mbdb.properties().atime()
					<<", ctime: "<<mbdb.properties().ctime()
					<<", protectionClass: "<<mbdb.properties().protectionclass();
			}
			
			int sizeOfKeyVal = mbdb.properties().keyvalues_size();
			if(sizeOfKeyVal>0)
			{
				for(int ii=0;ii<sizeOfKeyVal;ii++)
				{
					cout<<", key: "<<mbdb.properties().keyvalues(ii).key()
						<<", value: "<<(reinterpret_cast<const unsigned char*>(mbdb.properties().keyvalues(ii).value().c_str()),mbdb.properties().keyvalues(ii).value().length());
				}
			}
        }
        cnt += len;
	 }
	 google::protobuf::ShutdownProtobufLibrary();
	*/
	 
	/*
	char pb_1[] = {0x12, 0x07, 0x74, 0x65, 0x73, 0x74, 0x69, 0x6e, 0x67};
    char pb_2[] = {0x08, 0x96, 0x01};
    decode_protobuf(pb_1, pb_1+sizeof(pb_1), yield_cb);
    decode_protobuf(pb_2, pb_2+sizeof(pb_2), yield_cb);
	*/
  
	///******************************* CONTACT/
 
	//Prop("https://p20-contacts.icloud.com", "<A:propfind xmlns:A='DAV:'><A:prop><A:current-user-principal/></A:prop></A:propfind>");
	//Prop("https://p20-contacts.icloud.com/1260330658/principal", "<ns0:propfind xmlns:ns0=\"DAV:\" xmlns:ns1=\"urn:ietf:params:xml:ns:carddav\"><ns0:prop><ns1:addressbook-home-set /></ns0:prop></ns0:propfind>");
	//Prop("https://p34-contacts.icloud.com:443/1260330658/carddavhome","<ns0:propfind xmlns:ns0=\"DAV:\"><ns0:prop><ns0:resourcetype /></ns0:prop></ns0:propfind>");
	//Prop("https://p01-contacts.icloud.com:443/1260330658/carddavhome/card","<ns0:propfind xmlns:ns0=\"DAV:\"><ns0:prop><ns0:resourcetype /></ns0:prop></ns0:propfind>");
	//GetItem("https://p20-contacts.icloud.com:443/1260330658/carddavhome/card/RjE1OUQ3NzAtQjBDNy00RkM5LUFBQ0YtOEQzMEI3RjI0RUU0.vcf");

	cin.get();
	return 0;
}

