

#include <cstring>
#include <list>
#include <vector>

using namespace std;

class  DavAccessFactory {

	void initDav(static const string server, static const string username,static const string password);  //throws IOException,  Setup credentials and initialize
	virtual string propfind(string url, string propData, string depth) = 0 ;  // Make the webdav call needed to find particular properties
	virtual string findPrinciples()=0;  //Find the user's principal ID
	virtual string findHomeSet()=0; // Find the base HREF for the items  
	
	//TODO:  these last 3 need to return lists
	virtual string findUserGroupings()=0;  //  Find each calendar or contacts group
	//TODO:  find needs to be per group

	virtual string findItems()=0 ;// throws IOException, Find the items --- Calendars,  Contacts ...  per group 
	virtual vector<string> getItems()=0; // Get the item records --- Calendars,  Contacts ...
	virtual string getItem(string url)=0;
	virtual vector<string> getGroups()=0; //throws IOException,  Get the item groups 
	void clear();
};